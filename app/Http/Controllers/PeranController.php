<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Peran;
use App\Models\Film; 
use App\Models\Cast;// Tambahkan ini untuk mengimpor model Film



class PeranController extends Controller
{
    public function create($film_id)
    {
        $film = Film::find($film_id);
        $casts = Cast::all();   

        return view('peran.create', compact('film','casts'));
    }

    public function store (Request $request, $film_id)
    {
        $cast_id = $request->input('cast_id');
        Peran::create([
    		'cast_id' => $cast_id,
    		'film_id' => $film_id,
            'nama' => $request->nama,
            
    	]);

        return redirect('/film');  
    }
}
