<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profile;
use Auth;


class UserProfileController extends Controller
{
    public function profile()
    {
        $profile = Profile::where('user_id', Auth::id())->first();
       
        return view('user.profile', compact('profile'));
    }
}
