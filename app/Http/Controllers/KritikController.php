<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Models\Kritik;
use Auth;

class KritikController extends Controller
{
    public function store (Request $request, $film_id)
    {
        Kritik::create([
    		'user_id' => Auth::Id(),
    		'film_id' => $film_id,
            'content' => $request->content,
            'point' => $request->point,
            
    	]);

        return back();
    }
}
