@extends('layout.master')

@section('judul')
    Halaman Genre Film
@endsection

@section('content')
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>List Genre Film</title>
    </head>
    <body>
        <h2>List Genre Film</h2>

        @if($film)
            <p>Judul Film: {{ $film->judul }}</p>
            <p>Genre: {{ $film->genre->nama }}</p>

           
            <h3>Tambah Peran</h3>
            <form action="{{ route('peran.store', ['film_id' => $film->id]) }}" method="post">
                @csrf
                <div>
                    <label for="cast_id">Tokoh:</label>
                    <select name="cast_id" id="cast_id" required>
                        @foreach($casts as $cast)
                            <option value="{{ $cast->id }}">{{ $cast->nama }}</option>
                        @endforeach
                    </select>
                </div>
                <label>Peran:</label> <br> 
                <input type="text" name="nama"> ,<br> <br> 

                <button type="submit">Tambah</button>
            </form>

            <a href="{{ route('genre.index', ['genre_id' => $film->genre->id]) }}">Kembali ke halaman sebelumnya</a>
        @else
            <p>Data film tidak ditemukan.</p>
        @endif
    </body>
    </html>
@endsection
