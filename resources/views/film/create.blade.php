@extends('layout.master')

@section('judul')
Data Pemain Film
@endsection

@section('content')

 
    <form action="{{ route('film.store') }}" method="post">
        @csrf
        <label>Judul Film:</label> <br> <br>
        <input type="text" name="judul"> <br> <br>
        @error('judul')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror

        <label>Ringkasan:</label> <br> <br>
        <textarea name="ringkasan" cols="30" rows="10"></textarea> <br> <br>
        @error('ringkasan')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror

        <label>Tahun:</label> <br> <br>
        <input type="number" name="tahun"> <br> <br>
        @error('tahun')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror

        <label>Poster:</label> <br> <br>
        <input type="text" name="poster"> <br> <br>
        @error('poster')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror

        <label>Genre ID:</label> <br> <br>
        <input type="number" name="genre_id"> <br> <br>
        @error('genre_id')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
        <input type="submit" value="Tambah">

@endsection