@extends('layout.master')

@section('judul')
Detail Film
@endsection

@section('content')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
</head>
<body>
 

    @if($film)
        <p>Judul: {{ $film->judul }}</p>
        <p>Ringkasan: {{ $film->ringkasan }}</p>
        <p>Tahun: {{ $film->tahun }}</p>
        <p>Poster: {{ $film->poster }}</p>
        <p>Genre: {{ $film->genre->nama }}</p>
       
    @else
        <p>Data pemain film tidak ditemukan.</p>
    @endif
    <div class="card">
        <div class="card-header bg-primary">
            LIST KOMENTAR
        </div>
        <div class="card body">
        <form action="{{ route('kritik.store', ['film_id' => $film->id]) }}" method="post">
                @csrf
                <div class="card-body">
                  <h4>Komentar Pengguna:</h4>
                  @foreach ($film->kritik as $kritik)
                     <p><strong>{{ $kritik->user->name }}</strong>: {{ $kritik->content }} (Point: {{ $kritik->point }})</p>
                  @endforeach
                </div>
        </div>

        <div class="card body">
            <div class="form-group">
                <label for="content">Komentar:</label>
                <textarea name="content" rows="4" class="form-control" placeholder="Tambahkan komentar Anda"></textarea>
            </div>

            <div class="form-group">
                <label for="point">Point:</label>
                <input type="number" name="point" class="form-control" placeholder="Tambahkan point Anda">
            </div>
            <button type="submit">Tambah Komentar</button>
        </form>
    </div>
    <a href="{{ route('film.tampil', ['film_id' => $film->id]) }}">kembali ke halaman sebelumnya</a>
</body>
</html>
@endsection