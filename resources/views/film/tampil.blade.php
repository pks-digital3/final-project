@extends('layout.master')

@section('judul')
Data Film
@endsection

@section('content')
<a href="{{ route('film.create') }}" class="btn btn-primary mb-3">Tambah</a>
    <table class="table">
        <thead class="thead-light">
            <tr>
            <th scope="col">Nomor</th>
                <th scope="col">Judul</th>
                <th scope="col">Ringkasan</th>
                <th scope="col">Tahun</th>
                <th scope="col">Poster</th>
                <th scope="col">Genre</th>
                <th scope="col">Peran</th>
                <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($film as $key => $actor)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $actor->judul }}</td>
                    <td>{{ $actor->ringkasan }}</td>
                    <td>{{ $actor->tahun }}</td>
                    <td>{{ $actor->poster }}</td>
                    <td>{{ $actor->genre->nama }}</td>
                    <td>
                        @if ($actor->peran->isEmpty())
                            Belum ada peran
                        @else
                            @foreach ($actor->peran as $peran)
                                {{ $peran->nama }} ({{ $peran->cast->nama }})<br>
                            @endforeach
                        @endif
                    </td>
                    <td>
                        <div class="btn-group">
                            <a href="{{ route('film.show', ['film_id' => $actor->id]) }}" class="btn btn-info">Show</a>
                            <a href="{{ route('film.edit', ['film_id' => $actor->id]) }}" class="btn btn-primary">Edit</a>
                            <form action="{{ route('film.destroy', ['film_id' => $actor->id]) }}" method="POST" onsubmit="return confirm('Apakah Anda yakin ingin menghapus pemain ini?');">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger" value="Delete">
                            </form>
                            <a href="{{ route('peran.create', ['film_id' => $actor->id]) }}" class="btn btn-success">Tambah Peran</a>
                        </div>
                    </td>
                </tr>
            @empty
                <tr colspan="5">
                    <td>No data</td>
                </tr>
            @endforelse
        </tbody>
    </table>
@endsection