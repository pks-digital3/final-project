<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Detail Data Pemain Film</title>
</head>
<body>
    <h2>Detail Data Pemain Film</h2>

    @if($cast)
        <p>Nama: {{ $cast->nama }}</p>
        <p>Umur: {{ $cast->umur }}</p>
        <p>Bio: {{ $cast->bio }}</p>
        <a href="{{ route('cast.index', ['cast_id' => $cast->id]) }}">kembali ke halaman sebelumnya</a>
    @else
        <p>Data pemain film tidak ditemukan.</p>
    @endif
</body>
</html>
