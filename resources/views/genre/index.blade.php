@extends('layout.master')

@section('judul')
List Genre Film
@endsection

@section('content')
<a href="{{ route('genre.create') }}" class="btn btn-primary mb-3">Tambah</a>
    <table class="table">
        <thead class="thead-light">
            <tr>
                <th scope="col">Nomor</th>
                <th scope="col">ID</th>
                <th scope="col">Nama</th>
                <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($genre as $key => $actor)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $actor->id }}</td>
                    <td>{{ $actor->nama }}</td>
                    <td>
                     <div class="btn-group">
                        <a href="{{ route('genre.show', ['genre_id' => $actor->id]) }}" class="btn btn-info">Show</a>
                        <a href="/genre/{{$actor->id}}/edit" class="btn btn-primary">Edit</a>
                        <form action="{{ route('genre.destroy', ['genre_id' => $actor->id]) }}" method="POST" onsubmit="return confirm('Apakah Anda yakin ingin menghapus pemain ini?');">

                            @csrf
                            @method('DELETE')
                            <input type="submit" class="btn btn-danger" value="Delete">
                        </form>
                     </div>
                    </td>
                </tr>
            @empty
                <tr colspan="5">
                    <td>No data</td>
                </tr>
            @endforelse
        </tbody>
    </table>
@endsection