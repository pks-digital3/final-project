@extends('layout.master')

@section('judul')
Halaman Home
@endsection

@section('content')
    <h1>Profile</h1>
    <h2>Ini adalah Profilemu</h2>

    <p>Nama: {{ Auth::user()->name }}</p>
    <p>Email: {{ Auth::user()->email }}</p>
    
@endsection